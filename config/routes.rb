Rails.application.routes.draw do
  root 'static_pages#home'
  get 'root_path' => 'static_pages#home'
  get  'history' => 'static_pages#history'
  get 'people' => 'static_pages#people'
  get 'attractions' => 'static_pages#attractions'
  get 'comments' => 'messages#index'
  get 'about' => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'
  get 'tweet' => 'static_pages#posts'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :messages do
    resources :comments
  end
end

